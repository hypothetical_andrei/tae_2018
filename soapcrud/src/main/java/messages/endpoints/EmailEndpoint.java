package messages.endpoints;

import messages.entities.Email;
import messages.repositories.EmailRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ro.ase.andrei.message_service.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Endpoint
public class EmailEndpoint {
    private static final String URI = "http://andrei.ase.ro/message-service";
    private EmailRepository emailRepository;

    @Autowired
    public EmailEndpoint(EmailRepository repository){
        this.emailRepository = repository;
    }

    @PayloadRoot(namespace = URI, localPart = "getAllMessagesRequest")
    @ResponsePayload
    public GetAllMessagesResponse getMessages(){
        GetAllMessagesResponse response = new GetAllMessagesResponse();
        List<MessageInfo> messageInfoList = new ArrayList<>();
        List<Email> emails = emailRepository.findAll();
        for (Email email : emails){
            MessageInfo info = new MessageInfo();
            BeanUtils.copyProperties(email, info);
            messageInfoList.add(info);
        }
        response.getMessage().addAll(messageInfoList);
        return response;
    }

    @PayloadRoot(namespace = URI, localPart = "addMessageRequest")
    @ResponsePayload
    public AddMessageResponse addMessage(@RequestPayload AddMessageRequest request){
        AddMessageResponse response = new AddMessageResponse();
        Email email = new Email();
        email.setSource(request.getSource());
        email.setDestination(request.getDestination());
        email.setSubject(request.getSubject());
        email.setContent(request.getContent());
        emailRepository.save(email);
        MessageInfo info = new MessageInfo();
        BeanUtils.copyProperties(email, info);
        response.setMessageInfo(info);
        ServiceStatus status = new ServiceStatus();
        status.setStatusCode("SUCCESS");
        status.setMessage("added");
        response.setServiceStatus(status);
        return response;
    }

    @PayloadRoot(namespace = URI, localPart = "getMessageByIdRequest")
    @ResponsePayload
    public GetMessageByIdResponse getMessage(@RequestPayload GetMessageByIdRequest request){
        GetMessageByIdResponse response = new GetMessageByIdResponse();
        MessageInfo info = new MessageInfo();
        ServiceStatus status = new ServiceStatus();
        Optional<Email> emailOptional = emailRepository.findById(request.getMessageId());
        if (emailOptional.isPresent()){
            BeanUtils.copyProperties(emailOptional.get(), info);
            status.setMessage("found");
            status.setStatusCode("200");
        }
        else{
            status.setMessage("not found");
            status.setStatusCode("404");
        }
        response.setMessageInfo(info);
        return response;
    }

    @PayloadRoot(namespace = URI, localPart = "updateMessageRequest")
    @ResponsePayload
    public UpdateMessageResponse updateMessage(@RequestPayload UpdateMessageRequest request){
        UpdateMessageResponse response = new UpdateMessageResponse();
        MessageInfo info = new MessageInfo();
        ServiceStatus status = new ServiceStatus();
        Optional<Email> emailOptional = emailRepository.findById(request.getMessageId());
        if (emailOptional.isPresent()){
            Email email = emailOptional.get();
            BeanUtils.copyProperties(request.getMessageInfo(), email);
            emailRepository.save(email);
            status.setMessage("successfuly updated");
            status.setStatusCode("202");
        }
        else{
            status.setMessage("not found");
            status.setStatusCode("404");
        }
        return response;
    }

    @PayloadRoot(namespace = URI, localPart = "deleteMessageRequest")
    @ResponsePayload
    public DeleteMessageResponse deleteMessage(@RequestPayload DeleteMessageRequest request){
        DeleteMessageResponse response = new DeleteMessageResponse();
        ServiceStatus status = new ServiceStatus();
        Optional<Email> emailOptional = emailRepository.findById(request.getMessageId());
        if (emailOptional.isPresent()){
            Email email = emailOptional.get();
            emailRepository.delete(email);
            status.setMessage("successfuly deleted");
            status.setStatusCode("202");
        }
        else{
            status.setMessage("not found");
            status.setStatusCode("404");
        }
        return response;
    }
}
