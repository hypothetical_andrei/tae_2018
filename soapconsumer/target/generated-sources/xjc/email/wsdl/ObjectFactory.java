//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2018.05.17 at 07:28:23 PM EEST 
//


package email.wsdl;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the email.wsdl package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: email.wsdl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetAllMessagesRequest }
     * 
     */
    public GetAllMessagesRequest createGetAllMessagesRequest() {
        return new GetAllMessagesRequest();
    }

    /**
     * Create an instance of {@link GetAllMessagesResponse }
     * 
     */
    public GetAllMessagesResponse createGetAllMessagesResponse() {
        return new GetAllMessagesResponse();
    }

    /**
     * Create an instance of {@link MessageInfo }
     * 
     */
    public MessageInfo createMessageInfo() {
        return new MessageInfo();
    }

    /**
     * Create an instance of {@link GetMessageByIdRequest }
     * 
     */
    public GetMessageByIdRequest createGetMessageByIdRequest() {
        return new GetMessageByIdRequest();
    }

    /**
     * Create an instance of {@link GetMessageByIdResponse }
     * 
     */
    public GetMessageByIdResponse createGetMessageByIdResponse() {
        return new GetMessageByIdResponse();
    }

    /**
     * Create an instance of {@link AddMessageRequest }
     * 
     */
    public AddMessageRequest createAddMessageRequest() {
        return new AddMessageRequest();
    }

    /**
     * Create an instance of {@link AddMessageResponse }
     * 
     */
    public AddMessageResponse createAddMessageResponse() {
        return new AddMessageResponse();
    }

    /**
     * Create an instance of {@link ServiceStatus }
     * 
     */
    public ServiceStatus createServiceStatus() {
        return new ServiceStatus();
    }

    /**
     * Create an instance of {@link UpdateMessageRequest }
     * 
     */
    public UpdateMessageRequest createUpdateMessageRequest() {
        return new UpdateMessageRequest();
    }

    /**
     * Create an instance of {@link UpdateMessageResponse }
     * 
     */
    public UpdateMessageResponse createUpdateMessageResponse() {
        return new UpdateMessageResponse();
    }

    /**
     * Create an instance of {@link DeleteMessageRequest }
     * 
     */
    public DeleteMessageRequest createDeleteMessageRequest() {
        return new DeleteMessageRequest();
    }

    /**
     * Create an instance of {@link DeleteMessageResponse }
     * 
     */
    public DeleteMessageResponse createDeleteMessageResponse() {
        return new DeleteMessageResponse();
    }

}
