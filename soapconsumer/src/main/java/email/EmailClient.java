package email;

import email.wsdl.GetMessageByIdRequest;
import email.wsdl.GetMessageByIdResponse;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class EmailClient extends WebServiceGatewaySupport {

    public GetMessageByIdResponse getMessage(Long id){
        GetMessageByIdRequest request = new GetMessageByIdRequest();
        request.setMessageId(id);
        GetMessageByIdResponse response = (GetMessageByIdResponse) getWebServiceTemplate().marshalSendAndReceive("http://localhost:8080/ws", request);
        return response;
    }
}
