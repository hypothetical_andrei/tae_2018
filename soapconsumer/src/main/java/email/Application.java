package email;

import email.wsdl.GetMessageByIdResponse;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {
    public static void main(String[] args){
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner doLookupEmail(EmailClient client){
        return args -> {
          Long id = new Long(1);
            GetMessageByIdResponse response = client.getMessage(id);
            System.out.println(response.getMessageInfo().getContent());
        };
    }
}
