package crudconsumer.entities;

import lombok.Data;

@Data
public class Kitten {
    private long id;
    private String name;
    private String color;
}
