package crudconsumer;

import crudconsumer.entities.Kitten;
import org.springframework.web.client.RestTemplate;

public class Application {
    public static void main(String[] args){
        RestTemplate restTemplate = new RestTemplate();
        Kitten kitten = restTemplate.getForObject("http://localhost:8080/kittens/1", Kitten.class);
        System.out.println(kitten.getName());
    }
}
