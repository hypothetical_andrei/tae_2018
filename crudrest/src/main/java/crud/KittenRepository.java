package crud;

import crud.entities.Kitten;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "kittens", path = "kittens")
public interface KittenRepository extends PagingAndSortingRepository<Kitten, Long> {
    List<Kitten> findByColor(@Param("color") String color);
}
