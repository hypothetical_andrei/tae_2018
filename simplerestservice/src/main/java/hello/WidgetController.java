package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static org.springframework.http.ResponseEntity.accepted;

@RestController
@RequestMapping(value = "/widgets")
public class WidgetController {

    WidgetRepository repo;

    @Autowired
    public WidgetController(WidgetRepository repo) {
        this.repo = repo;
    }

    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Widget> getWidgets() {
        return repo.findAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Optional<Widget>> getWidget(@PathVariable Long id) {
        Optional<Widget> w = repo.findById(id);
        if (w != null){
            return ResponseEntity.ok(w);
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> postWidget(@RequestBody Widget widget) {
        System.err.println(widget);
        repo.save(widget);
        return accepted().build();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> putWidget(@PathVariable Long id, @RequestBody Widget widget) {
        Optional<Widget> w = repo.findById(id);
        Widget oldWidget = w.get();
        if (oldWidget != null) {
            oldWidget.setName(widget.getName());
            repo.save(oldWidget);
            return ResponseEntity.accepted().build();
        } else {
            return ResponseEntity.notFound().build();
        }

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteWidget(@PathVariable Long id) {
        Optional<Widget> w = repo.findById(id);
        Widget oldWidget = w.get();
        repo.delete(oldWidget);
        return ResponseEntity.accepted().build();
    }
}
