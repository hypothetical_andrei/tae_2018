package hello;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface WidgetRepository  extends CrudRepository<Widget, Long>{
    List<Widget> findByName(String name);
}


