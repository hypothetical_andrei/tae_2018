package hello.controllers;

import hello.entities.Widget;
import hello.repositories.WidgetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/widgets")
public class WidgetController {
    WidgetRepository repo;

    @Autowired
    public WidgetController(WidgetRepository repo){
        this.repo = repo;
    }

    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Widget> getWidgets(){
        return repo.findAll();
    }

    @RequestMapping(value= "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Optional<Widget>> getWidget(@PathVariable Long id){
        Optional<Widget> widget = repo.findById(id);
        if (widget.isPresent()){
            return ResponseEntity.ok(widget);
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<?> postWidget(@RequestBody Widget widget){
        repo.save(widget);
        return ResponseEntity.accepted().build();
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> putWidget(@PathVariable Long id, @RequestBody Widget widget) {
        Optional<Widget> w = repo.findById(id);
        if (w.isPresent()) {
            Widget oldWidget = w.get();
            oldWidget.setName(widget.getName());
            oldWidget.setType(widget.getType());
            repo.save(oldWidget);
            return ResponseEntity.accepted().build();
        } else {
            return ResponseEntity.notFound().build();
        }

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteWidget(@PathVariable Long id){
        Optional<Widget> w = repo.findById(id);
        if (w.isPresent()){
            repo.delete(w.get());
            return ResponseEntity.accepted().build();
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }

}
