package hello.repositories;

import hello.entities.Kitten;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(path="kittens")
public interface KittenRepository extends PagingAndSortingRepository<Kitten, Long> {
    List<Kitten> findByColor(@Param("color") String color);
}
