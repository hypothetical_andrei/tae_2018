package hello;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Greeting {
//
//    public Greeting(long id,String content) {
//        this.id = id;
//        this.content = content;
//    }

    private long id;
    private String content;

}
