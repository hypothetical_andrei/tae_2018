package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ro.ase.andrei.greeting_service.GetGreetingRequest;
import ro.ase.andrei.greeting_service.GetGreetingResponse;

@Endpoint
public class GreetingEndpoint {
    private static final String NAMESPACE_URI="http://andrei.ase.ro/greeting-service";
    private GreetingRepository greetingRepository;

    @Autowired
    public GreetingEndpoint(GreetingRepository greetingRepository){
        this.greetingRepository = greetingRepository;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getGreetingRequest")
    @ResponsePayload
    public GetGreetingResponse getGreeting(@RequestPayload GetGreetingRequest request){
        GetGreetingResponse response = new GetGreetingResponse();
        response.setGreeting(greetingRepository.getGreeting(request.getName()));
        return response;
    }
}
