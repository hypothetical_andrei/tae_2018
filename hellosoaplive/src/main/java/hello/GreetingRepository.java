package hello;

import org.springframework.stereotype.Component;
import ro.ase.andrei.greeting_service.Greeting;

@Component
public class GreetingRepository {
    public Greeting getGreeting(String name){
        Greeting g = new Greeting();
        g.setContent("hello " + (!name.isEmpty() ? name : "world"));
        return g;
    }
}
